export default class BlobReader {
  constructor (blob) {
    this.blob = blob
  }
  parseByte (startByte, endByte) {
    return new Promise(resolve => {
      var start = startByte
      var stop = endByte
      var reader = new FileReader()
      reader.onloadend = (evt) => {
        resolve(evt.target.result)
      }
      var blob = this.blob.slice(start, stop + 1)
      reader.readAsBinaryString(blob)
    })
  }
}
