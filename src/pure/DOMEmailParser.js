import Moment from './moment'
export default class DOMEmailParser {
  constructor (el) {
    if (typeof el !== 'object') {
      var parser = new DOMParser()
      el = parser.parseFromString(this.trimCustom(el), 'text/html')
    }
    this.el = el
    this.moment = new Moment(el.textContent.trim())
  }
  agregateTrip () {
    if (this.el) {
      var tripInfo = this.getTripInfo()
      var price = this.getTripPrice()
      var roundTrips = this.getRoundTrips()
      var custom = this.getCustom()
      var trip = {
        ...tripInfo,
        details: {
          price,
          roundTrips
        },
        custom
      }
      return {
        status: 'ok',
        result: {
          trips: [
            trip
          ]
        }
      }
    }
  }
  priceParser (stringPrice) {
    var re = /[1-9]*,[0-9]\d/
    return parseFloat(re.exec(stringPrice)[0].replace(',', '.'))
  }
  getTripInfo () {
    return {
      code: this.el.querySelector('.digital-box-cell > .block-pnr .pnr-ref .pnr-info').textContent.trim(),
      name: this.el.querySelector('.digital-box-cell > .block-pnr .pnr-name .pnr-info').textContent.trim()
    }
  }
  getTripPrice () {
    var price = this.el.querySelector('#block-payment .total-amount').textContent.trim()
    return this.priceParser(price)
  }
  getTrainsFromOneWayTrip ($details, flagPassengers) {
    var $trains = $details.querySelectorAll('tr')
    var trains = []
    let currentTrainIdx = 0
    ;[].map.call($trains, (item, key) => {
      if (key % 2 === 0) {
        var $station = item.querySelector('.origin-destination-station')
        var $type = $station.nextElementSibling
        trains[currentTrainIdx] = {}
        trains[currentTrainIdx].departureStation = $station.textContent.trim()
        trains[currentTrainIdx].departureTime = item.querySelector('.origin-destination-hour').textContent.trim()
        trains[currentTrainIdx].type = $type.textContent.trim()
        trains[currentTrainIdx].number = $type.nextElementSibling.textContent.trim()
      } else {
        trains[currentTrainIdx].arrivalStation = item.querySelector('.origin-destination-station').textContent.trim()
        trains[currentTrainIdx].arrivalTime = item.querySelector('.origin-destination-hour').textContent.trim()
        currentTrainIdx++
      }
    })
    if (flagPassengers) {
      trains[trains.length - 1].passengers = this.getPassengersFromOneWayTrip($details.nextElementSibling)
    }
    return trains
  }
  getPassengersFromOneWayTrip ($details) {
    var $tr = $details.querySelectorAll('tr')
    var $passengers = []
    var passengers = []
    ;[].map.call($tr, (item, key) => {
      if (item.textContent.trim().replace(/\s/g, '').length) {
        $passengers.push(item)
      }
    })
    $passengers.map((item, key) => {
      var age = item.querySelector('.typology').textContent.trim()
      var re = /[0-9]*\sà\s[0-9]*\sans/
      age = re.exec(age)[0]
      var type = (item.textContent.trim().indexOf('Billet échangeable et remboursable') > -1) ? 'échangeable' : 'non échangeable'
      passengers[key] = {
        age,
        type
      }
    })
    return passengers
  }
  getRoundTrips () {
    var subTrips = this.el.querySelectorAll('#block-command .digital-box-cell > .product-header')
    var trips = []
    ;[].map.call(subTrips, (item, key) => {
      var $day = item.nextElementSibling
      var $details = $day.nextElementSibling
      var $passengers = $details.nextElementSibling
      var $returnDay = $passengers.nextElementSibling

      var day = $day.querySelector('.product-travel-date').textContent.trim()
      let trains = this.getTrainsFromOneWayTrip($details, $returnDay.classList.contains('.product-spacer'))
      var details = {
        type: $details.querySelector('.travel-way').textContent.trim(),
        date: this.moment.dateHumanFormatToDateString(day),
        trains: [
          ...trains
        ]
      }
      trips.push(details)
      if (!$returnDay.classList.contains('.product-spacer')) {
        var $returnDetails = $returnDay.nextElementSibling
        var returnDay = $returnDay.querySelector('.product-travel-date').textContent.trim()

        let trains = this.getTrainsFromOneWayTrip($returnDetails, key === subTrips.length - 1)
        var returnDetails = {
          type: $returnDetails.querySelector('.travel-way').textContent.trim(),
          date: this.moment.dateHumanFormatToDateString(returnDay),
          trains: [
            ...trains
          ]
        }
        trips.push(returnDetails)
      }
    })
    return trips
  }
  trimCustom (htmlString) {
    return decodeURIComponent(escape(htmlString)).replace(/\\"/g, '"').replace(/\\r\\n/g, '')
  }
  getPricesHeader () {
    var $pricesHeader = this.el.querySelectorAll('#block-command .digital-box-cell .product-header')
    var prices = []
    ;[].map.call($pricesHeader, (item, key) => {
      var price = this.priceParser(item.textContent.trim())
      prices.push({value: price})
    })
    return prices
  }
  getCustom () {
    var prices = this.getPricesHeader()
    return {
      prices
    }
  }
}
