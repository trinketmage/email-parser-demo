export default class moment {
  constructor (data) {
    this.year = this.findContextYear(data)
    this.days = [
      'Dimanche',
      'Lundi',
      'Mardi',
      'Mercredi',
      'Jeudi',
      'Vendredi',
      'Samedi'
    ]
    this.months = [
      'Janvier',
      'Février',
      'Mars',
      'Avril',
      'Mai',
      'Juin',
      'Juillet',
      'Août',
      'Septembre',
      'Octobre',
      'Novembre',
      'Décembre'
    ]
  }
  findContextYear (data) {
    var re = /[0-9]{2}\/[0-9]{2}\/[0-9]{4}/
    var date = re.exec(data)[0]
    date = date.split('/')[2]
    return date
  }
  dateHumanFormatToDateString (date) {
    var keyMonth = this.months.find((item, key) => {
      return date.indexOf(item) > -1
    })
    var month = this.months.indexOf(keyMonth)
    var re = /[0-9]*\d/
    var dayOfMonth = re.exec(date)[0]
    var event = new Date()
    event.setHours(0, 0, 0, 0)
    event.setDate(dayOfMonth)
    event.setMonth(month)
    event.setYear(this.year)
    return event.toISOString()
  }
}
